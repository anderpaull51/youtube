const { Builder, By, Key, until } = require("selenium-webdriver");

describe("Suma de dos números", function() {
  let driver;

  beforeEach(async function() {
    driver = await new Builder().forBrowser("chrome").build();
  });

  afterEach(async function() {
    await driver.quit();
  });

  it("debe sumar dos números y mostrar el resultado", async function() {
    await driver.get("http://tu-pagina-web.com");

    const num1 = await driver.findElement(By.name("num1"));
    const num2 = await driver.findElement(By.name("num2"));
    const sumar = await driver.findElement(By.tagName("button"));

    await num1.sendKeys("2");
    await num2.sendKeys("3");
    await sumar.click();

    const resultado = await driver.findElement(By.id("resultado")).getText();
    expect(resultado).toEqual("El resultado es: 5");
  });
});
